import React, { Component } from "react";
import { loadModels, getFullFaceDescription } from "../apis/faceapi";
import Webcam from "react-webcam";
import styles from "./faceApp.module.css";

const WIDTH = 420;
const HEIGHT = 420;
const inputSize = 160;

const videoConstraints = {
  width: WIDTH,
  height: HEIGHT,
  facingMode: "user"
};

class FaceDetect extends Component {
  constructor(props) {
    super(props);
    this.webcam = React.createRef();
  }

  state = {
    detections: [],
    loading: false
  };
  async componentWillMount() {
    console.log("Loading Models");
    await loadModels();
    this.capture();
  }

  componentWillUnmount() {
    clearInterval(this.interval);
    clearTimeout(this.timeOutInterval);
  }

  capture = async () => {
    this.setState({
      loading: true
    });
    if (!!this.webcam.current) {
      const screenshot = this.webcam.current.getScreenshot();
      this.currentScreenshot = screenshot;

      await getFullFaceDescription(screenshot, inputSize).then(fullDesc => {
        //Handle for multiple faces
        if (fullDesc.length == 1) {
          console.log(fullDesc);
          this.setState({
            detections: fullDesc,
            loading: false
          });
        } else if (fullDesc.length > 1) {
          console.error("More than one face is present.");
        } else {
          this.timeOutInterval = setTimeout(() => {
            this.capture();
          }, 1500);
        }
      });
    }
  };

  renderOnScreenMessageOnCanvas() {
    return (
      <div className={styles.overLayImage}>
        <img src={this.currentScreenshot} />
      </div>
    );
  }
  render() {
    let drawBox = null;
    const { detections } = this.state;

    if (!!detections) {
      drawBox = detections.map((detection, i) => {
        let _H = detection.box.height;
        let _W = detection.box.width;
        let _X = detection.box._x;
        let _Y = detection.box._y;
        return (
          <div key={i}>
            <div
              style={{
                position: "absolute",
                border: "solid",
                borderColor: "blue",
                height: _H,
                width: _W,
                zIndex: 3,
                transform: `translate(${_X}px,${_Y}px)`
              }}
            ></div>
          </div>
        );
      });
    }

    return (
      <div className={styles.mainDiv}>
        <div style={{ position: "relative", width: WIDTH }}>
          {this.renderOnScreenMessageOnCanvas()}
          <div style={{ position: "absolute" }}>
            <Webcam
              audio={false}
              height={HEIGHT}
              ref={this.webcam}
              screenshotFormat="image/jpeg"
              width={WIDTH}
              videoConstraints={videoConstraints}
            />
          </div>
          {!!drawBox ? drawBox : null}
        </div>
      </div>
    );
  }
}

export default FaceDetect;
