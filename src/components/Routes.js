import React from "react";
import { Route } from "react-router-dom";

import Page1 from "./pages/page1";
import Page2 from "./pages/page2";
import FaceDetector from "./faceDetectAlgo";

const Routes = () => {
  return (
    <React.Fragment>
      <Route path="/" exact component={FaceDetector} />
      <Route path="/page1" exact component={Page1} />
      <Route path="/page2" exact component={Page2} />
    </React.Fragment>
  );
};

export default Routes;
