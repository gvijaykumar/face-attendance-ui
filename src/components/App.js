import React from "react";
import { withRouter } from "react-router-dom";
import Routes from "./Routes";

import "./App.css";

const App = props => {
  return (
    <>
      <span>I am header</span>
      <Routes />
    </>
  );
};

export default withRouter(App);
