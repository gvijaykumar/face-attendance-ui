import * as faceapi from "face-api.js";

const MODEL_URL = "/models";
const maxDescriptorDistance = 0.5;
const minConfidence = 0.6;

// Load models and weights
export async function loadModels() {
  //   await Promise.all([
  //     faceapi.loadTinyFaceDetectorModel(MODEL_URL),
  //     faceapi.loadFaceRecognitionModel(MODEL_URL)
  //   ]);
  await faceapi.loadTinyFaceDetectorModel(MODEL_URL);
  await faceapi.loadFaceRecognitionModel(MODEL_URL);
}

export async function getFullFaceDescription(blob, inputSize = 512) {
  // tiny_face_detector options
  let scoreThreshold = 0.5;
  const OPTION = new faceapi.TinyFaceDetectorOptions({
    inputSize,
    scoreThreshold
  });
  const useTinyModel = true;

  // fetch image to api
  let img = await faceapi.fetchImage(blob);

  // detect all faces and generate full description from image
  // including landmark and descriptor of each face
  let fullDesc = await faceapi.detectAllFaces(img, OPTION);
  return fullDesc;
}
