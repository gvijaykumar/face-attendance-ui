import { combineReducers } from "redux";

export default combineReducers({
  removeMe: () => "remove me"
});
